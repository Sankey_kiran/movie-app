import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieListingComponent } from './movie-app/movie-listing/movie-listing.component';
import { MovieDetailsComponent } from './movie-app/movie-details/movie-details.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'movie-list' },
	{ path: 'movie-list', component: MovieListingComponent },
	{ path: 'movie-details/:movieId', component: MovieDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
