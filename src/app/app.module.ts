import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule} from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MovieListingComponent } from './movie-app/movie-listing/movie-listing.component';
import { MovieDetailsComponent } from './movie-app/movie-details/movie-details.component';
import { HeaderComponent } from './shared/header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MovieDetailsService } from './services/movie-details.service';

@NgModule({
  declarations: [
    AppComponent,
    MovieListingComponent,
    MovieDetailsComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    AngularEditorModule,
    FormsModule, 
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    ReactiveFormsModule, BrowserAnimationsModule
  ],
  providers: [MovieDetailsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
