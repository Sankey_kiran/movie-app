import { Component, OnInit } from '@angular/core';
import {Images} from '../../config/image-config';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
image = Images;
  constructor() { }

  ngOnInit(): void {
  }

}
