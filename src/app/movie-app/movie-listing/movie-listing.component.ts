import { Component, OnInit } from '@angular/core';
import { MovieDetailsService } from 'src/app/services/movie-details.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-listing',
  templateUrl: './movie-listing.component.html',
  styleUrls: ['./movie-listing.component.scss']
})
export class MovieListingComponent implements OnInit {
movieList:[]
  constructor(
    private movieDetailsService:MovieDetailsService,
    private router:Router
  ) { }

  ngOnInit() {
    this.getList();
  }
  getList(){
    this.movieDetailsService.getMovieList()
    .subscribe((data)=>{
      this.movieList = data['genres'];
      try{
        if(this.movieList){
          this.movieList.forEach((element, index) =>{
            this.getGenreList(index, element);
          })
          
          
      }
      }
      catch(err){
        console.log("error", err);
      }

    })
  }
  getGenreList(i, element){
    this.movieDetailsService.getGenreMovie(element['id'])
    .subscribe((genreData)=>{
        this.movieList[i] = Object.assign(this.movieList[i], {genreMovieList:genreData['results']});
    })
    
  }
  redirectToDetailScreen(movie){
    this.router.navigate(['/movie-details/123']);
  }
}
