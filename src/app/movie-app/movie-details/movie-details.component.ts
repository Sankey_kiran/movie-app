import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MovieDetailsService } from 'src/app/services/movie-details.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {

  constructor(
    private router:Router,
    private activeParams: ActivatedRoute,
    private movieDetailsService:MovieDetailsService
  ) { }

  ngOnInit(): void {
    this.activeParams.params.subscribe(data =>{
      this.movieDetailsService.getMovieDetails(data.movieId)
      .subscribe((genreData)=>{
        console.log("genreData",genreData);
        // this.movieDetails= 
          // element['genreMovieList'] = genreData;
      })
    })
   
  
  }
  redirectToMovieList(){
    this.router.navigate(['/movie-listing']);

  }
}
