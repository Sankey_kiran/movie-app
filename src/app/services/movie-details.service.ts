import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MovieDetailsService {
  MOVIE_LISTING_URL = 'https://api.themoviedb.org/3/genre/movie/list';
  MOVIE_GENRE_URL = 'https://api.themoviedb.org/3/genre/';
  MOVIE_DETAILS_URL = 'https://api.themoviedb.org/3/movie/>';
  API_KEY = '?api_key=68e82445c8842ba8616d0f6bf0e97a41'
  HTTP_OPTIONS = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET'
         })
  };
  constructor(
    private http: HttpClient) { }

  getMovieList(){
    return this.http.get(this.MOVIE_LISTING_URL+this.API_KEY, this.HTTP_OPTIONS)
  }

  getGenreMovie(id){
    let UPMOVIE_GENRE_URL = this.MOVIE_GENRE_URL + id +'/movies';
    return this.http.get(UPMOVIE_GENRE_URL+this.API_KEY, this.HTTP_OPTIONS)
  }

  getMovieDetails(id){
    let UPMOVIE_DETAILS_URL = this.MOVIE_DETAILS_URL + id;
    return this.http.get(UPMOVIE_DETAILS_URL+this.API_KEY, this.HTTP_OPTIONS)

  }
}
